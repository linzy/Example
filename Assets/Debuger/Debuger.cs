﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuger {
    
    public static bool EnableLog = false;
    public static void Log(object msg,Object context)
    {
        if (EnableLog)
        {
            Debug.Log(msg, context);
        }
    }

    public static void LogError(object msg)
    {
        LogError(msg, null);
    }
    public static void LogError(object msg,Object context)
    {
        if (EnableLog)
        {
            Debug.LogError(msg, context);
        }
    }

    public static void LogFormat(string str,params object[] objs)
    {
        Debug.LogFormat(str, objs);
    }
}
