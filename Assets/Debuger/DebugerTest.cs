﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugerTest : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Object o = new Object();

        Debuger.EnableLog = true;
        Debuger.Log("Hello Bug",null);

        Debug.Log("msg", o);

        //Debuger.Log(string.Format("{0}{1}{2}{3}{4}", 1, 2, 3, 4, 5));
    }

    private void Update()
    {
        Debug.Log("Update:"+Time.fixedDeltaTime);
    }

}