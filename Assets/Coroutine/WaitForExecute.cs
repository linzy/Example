﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForExecute : MonoBehaviour {
    

    float time = 0;

    bool isTrue = true;

	// Use this for initialization
	void Start () {


        //StartCoroutine(cacul());

        //IEnumerator c = cacul();
        //while (c.MoveNext())
        //{
        //    Debug.Log(c.Current);
        //}

        StartCoroutine(cacul(CallBack));
	}


    public void CallBack(int result)
    {
        Debug.Log("callback result:" + result);
    }


    IEnumerator cacul(CallBack<int> callback)
    {
        for (int i = 0; i < 1000; i++)
        {
            //Debug.Log(i);
            if (time > 0.1f)
            {
                yield return new WaitForEndOfFrame();
                time = 0;
            }
            else
            {
                yield return i;
                callback(i);
            }
        }

        //if (time > 0.2f)
        //yield return 
    }

    private void testFunc()
    {

        while (isTrue)
        {
            for(int i = 0; i < 1000; i++)
            {
                Debug.Log("testFunc i:" + i);
            }
        }

    }

	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        Debug.Log("Time:" + time);

        //if (time > 0.01f)
        //    time = 0;
	}
}
