﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// https://msdn.microsoft.com/zh-cn/library/system.collections.ienumerator.aspx

public class CoroutineExample : MonoBehaviour {

    ArrayList list = new ArrayList();
    List<int> intlist = new List<int>();

	void Start () {

        // 使用foreach遍历迭代器
        foreach (int i in Integers())
        {
            Debug.Log(i.ToString());
        }

        // 使用MoveNext方法遍历迭代器
        IEnumerator e = Step();
        while (e.MoveNext())
        {
            Debug.Log(e.Current);
        }

        // 普通方法计算斐波纳契数列(Fibonacci Sequence)
        fab(5);

        IEnumerator e2 = fabIEnume(5);
        while (e2.MoveNext())
        {
            Debug.Log(e2.Current);
        }

        // MonoBehaviour StartCoroutine
        Debug.Log("=== MonoBehaviour StartCoroutine ===");
        StartCoroutine(fabIEnume(10));

        // TaskManager Example
        Debug.Log("=== TaskManager ===");
        Task t = new Task(fabIEnume(10));
        // TaskManager FinishedHandler
        t.Finished += T_Finished;

        // Use CallBack
        Task t2 = new Task(fabIEnume(CallBack,10));
    }

    public void CallBack(int result)
    {
        Debug.Log("callback result:" + result);
    }

    private void T_Finished(bool manual)
    {
        Debug.Log("=== TaskManager Finish ===");
    }

    public IEnumerable<int> Integers()
    {
        yield return 1;
        yield return 2;
        yield return 4;
        yield return 8;
        yield return 16;
        yield return 32;
    }

    IEnumerator Step()
    {
        Debug.Log("1");
        Debug.Log("2");
        yield return null;
        Debug.Log("1-1");
        Debug.Log("1-2");
        yield return null;
        Debug.Log("2-1");
        Debug.Log("2-2");
        yield return null;
    }

   public void fab(int max)
    {
        int n = 0, a = 0, b = 0,c=1;
        while (n < max)
        {
            Debug.Log(c);
            a = b;
            b = c;
            c = a + c;
            n++;
        }
    }
    

    IEnumerator fabIEnume(int max)
    {
        int n = 0, a = 0, b = 0, c = 1;
        while (n < max)
        {
            yield return c;
            a = b;
            b = c;
            c = a + c;
            n++;
        }
    }

    IEnumerator fabIEnume(CallBack<int> callback,int max)
    {
        int n = 0, a = 0, b = 0, c = 1;
        while (n < max)
        {
            yield return c;
            callback(c);
            a = b;
            b = c;
            c = a + c;
            n++;
        }
    }
}
