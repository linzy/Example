﻿
public delegate void CallBack();
public delegate void CallBack<A>(A arg1);
public delegate void CallBack<A, B>(A arg1, B arg2);
public delegate void CallBack<A, B, C>(A arg1, B arg2, C arg3);